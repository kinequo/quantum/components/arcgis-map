


class ArcGisMap extends HTMLElement {

  constructor() {
    super();
    // this.attachShadow({mode: 'open'});
    this._loadMap = this._loadMap.bind(this);
  }

  connectedCallback() {
    let script = this.ownerDocument.getElementById('ARGIS_SCRIPT');
    if (script == null) {
      script = this._addScriptToDocument();
    }
    this._container = this.ownerDocument.createElement('div');
    this._container.style.width = '100%';
    this._container.style.height = '100%';
    this.appendChild(this._container);
    if (script.scriptLoaded) {
      this._loadMap()
    } else {
      const loadListener = () => {
        this._loadMap();
        script.removeEventListener('load', loadListener);
      };
      script.addEventListener('load', loadListener);
    }
  }
  _addScriptToDocument() {
    const stylesheet = this.ownerDocument.createElement('link');
    stylesheet.rel = 'stylesheet';
    stylesheet.href = 'https://js.arcgis.com/4.10/esri/css/main.css';
    const arcgisScript = this.ownerDocument.createElement('script');
    arcgisScript.id = 'ARGIS_SCRIPT';
    arcgisScript.src = 'https://js.arcgis.com/4.10/';
    arcgisScript.addEventListener('load', () => {
      arcgisScript.scriptLoaded = true;
    });
    this.ownerDocument.head.appendChild(stylesheet);
    this.ownerDocument.head.appendChild(arcgisScript);
    return arcgisScript;
  }

  // PUBLIC API

  
  // PRIVATE API
  _loadMap() {
    this.ownerDocument.defaultView.require([
      "esri/Map",
      "esri/views/MapView",
      "esri/Graphic",
      "esri/layers/TileLayer",
      "esri/geometry/Extent",
      "esri/geometry/SpatialReference",
    ], (Map, MapView, Graphic, TileLayer, Extent, SpatialReference) => {
      var map = new Map({
        basemap: "gray",
      });

      // const url = 'https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer';
      // map.layers.add(new TileLayer({
      //   url,
      //   // minScale:,
      //   // maxScale
      // }));

      this._extent = new Extent({
        xmax: -3.24,
        xmin: -4.14,
        ymax: 40.61,
        ymin: 40.25,
        spatialReference: new SpatialReference(4326)
      });
    
      var view = new MapView({
        container: this._container,
        map: map,
        extent: this._extent,
        ui: {
          // components: [ "attribution" ]
          components: []
        }
      });

      this._map = map;
      this._view = view;
      this._Graphic = Graphic;

      this.dispatchEvent(new CustomEvent('arcgis-map-load', {
        bubbles: true,
        composed: true
      }));
    });
  }
}

// TODO: revisar para quitar window y poner el window del contexto
window.customElements.define('arcgis-map', ArcGisMap);