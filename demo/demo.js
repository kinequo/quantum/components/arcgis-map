// https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer

let DotServices = {};
// El código del método DotServices.busOTP es el siguiente:
DotServices.busOTP = function (from, to, arriveBy, time, date, walkDistance, mode, onSuccess) {
  var data = {
    fromPlace: '40.45297000000005,-3.6972699999999468',
    toPlace: '40.397876999999994,-3.4891717500000086',
    time: '05:41PM',
    date: '2-21-2019',
    maxWalkDistance: 500,
    mode: 'TRANSIT,WALK',
    arriveBy: false,
    showIntermediateStops: false,
    transferPenalty: 120
  };
  // DotServices.post(DARB.WEB_API_URL + '/bus/otp', data, onSuccess);
  axios.post('http://pilotocrtm.westeurope.cloudapp.azure.com/dotservices/api/bus/otp', data)
    .then((arg1, arg2, arg3) => {
      console.log(arg1, arg2, arg3);
      
      renderRoute(arg1.data.plan. itineraries[0]);
    });
}
DotServices.busOTP();

function renderRoute(itinerary) {
  
  
  // routeLayer.setRenderer(new esri.renderer.UniqueValueRenderer(new esri.symbol.SimpleLineSymbol().setColor(new esri.Color([0, 0, 255, 0.5])).setWidth(4), 'mode'));
  // routeLayer.renderer.addValue('WALK', new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_DOT, new esri.Color([0, 0, 255, .5]), 3));
  // routeLayer.renderer.addValue('SUBWAY', new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_DASH, new esri.Color([0, 0, 255, .5]), 3));
  // routeLayer.renderer.addValue('RAIL', new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_DASHDOT, new esri.Color([0, 0, 255, .5]), 3));

  // var stopsLayer = this.selected().stopsLayer = visor.map.addLayer(new esri.layers.GraphicsLayer());
  // stopsLayer.setRenderer(new esri.renderer.UniqueValueRenderer(visor.map.smallMarkerSymbol, 'isSelected', 'mode', '', ','));
  // stopsLayer.renderer.addValue('true,BUS', new esri.symbol.PictureMarkerSymbol({ "yoffset": 15, "xoffset": 0, "url": "./img/icons/map-poi/bus-stop.png", "width": 30, "height": 30 }));
  // stopsLayer.renderer.addValue('true,SUBWAY', new esri.symbol.PictureMarkerSymbol({ "yoffset": 15, "xoffset": 0, "url": "./img/icons/map-poi/subway.png", "width": 30, "height": 30 }));
  // stopsLayer.renderer.addValue('true,RAIL', new esri.symbol.PictureMarkerSymbol({ "yoffset": 15, "xoffset": 0, "url": "./img/icons/map-poi/rail.png", "width": 30, "height": 30 }));
  const colors = [
    'blue', 'green', 'red', 'yellow', 'black', 'lightblue', 'pink', 
  ]
  itinerary.legs.forEach((leg, i) => {
      var points = decodeLegPath(leg.legGeometry.points);

      var polyline = {
        type: "polyline",
        spatialReference: { "wkid": 4326 },
        paths: [points]
      };
      const lineSymbol = {
        type: 'simple-line',
        width: 4
      };
      if (leg.mode === 'WALK') {
        lineSymbol.style = 'short-dot';
        lineSymbol.color = 'gray';
      } else {
        lineSymbol.color = `#${leg.routeColor}`;
      }
      console.log(lineSymbol.color);
      var polylineGraphic = new bigMap._Graphic({
        geometry: polyline,
        symbol: lineSymbol,
        attributes: {}
      });
    
      // Add the graphic to the view's default graphics view
      // If adding multiple graphics, use addMany and pass in the array of graphics.
      bigMap._view.graphics.add(polylineGraphic);
      // leg.graphic = routeLayer.add(new bigMap._Graphic(new esri.geometry.Polyline(polyline), null, { mode: leg.mode }));
      // if (leg.from.stopId && leg.to.stopId) {
      //     stopsLayer.add(new esri.Graphic(new esri.geometry.Point(leg.from.lon, leg.from.lat), null, { isSelected: true, mode: leg.mode }));
      //     stopsLayer.add(new esri.Graphic(new esri.geometry.Point(leg.to.lon, leg.to.lat), null, { isSelected: true, mode: leg.mode }));
      // }
  });

  bigMap._view.goTo(bigMap._view.graphics)
}

function decodeLegPath(polyline) {
  var currentPosition = 0, currentLat = 0, currentLng = 0, dataLength = polyline.length, polylineLatLngs = [];

  while (currentPosition < dataLength) {
      var shift = 0, result = 0, byte;

      do {
          byte = polyline.charCodeAt(currentPosition++) - 63;
          result |= (byte & 0x1f) << shift;
          shift += 5;
      } while (byte >= 0x20);

      var deltaLat = ((result & 1) ? ~(result >> 1) : (result >> 1));
      currentLat += deltaLat;
      shift = 0;
      result = 0;

      do {
          byte = polyline.charCodeAt(currentPosition++) - 63;
          result |= (byte & 0x1f) << shift;
          shift += 5;
      } while (byte >= 0x20);

      var deltLng = ((result & 1) ? ~(result >> 1) : (result >> 1));
      currentLng += deltLng;
      polylineLatLngs.push([currentLng * 0.00001, currentLat * 0.00001]);
  }
  return polylineLatLngs;
}

setTimeout(() => {
  bigMap.getCandidates("adela balboa");
}, 1000);

//Estaciones
//http://geoespacial.idom.com:8080/arcgis106/rest/services/CRTM/Locator/GeocodeServer

//Direcciones:
//http://geocode.arcgis.com/arcgis/rest/services/World/GeocodeServer

//   getAddress: function (point, distance) {
//     var deferred = new dojo.Deferred();
//     try {
//       this.geoLocator.locationToAddress(point, distance || 25, function (result) {
//           deferred.resolve(result);
//         }, function (error) {
//           deferred.resolve(null);
//         });
//       return deferred.promise;
//     } catch (ex) {
//       deferred.resolve(null);
//     }
//   }
// };